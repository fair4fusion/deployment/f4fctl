# f4fctl

Command-line interface to the
[Fair-for-Fusion REST API](https://gitlab.com/fair4fusion/deployment/f4f-rest-api)


## Get Access Token

Access http://rest-operator.fair4fusion.iit.demokritos.gr/get_token
either while previously logged in Gitlab.com or sign in using
**gitlab-f4f** option.

You will be redirected to a page that contains a json. Find the
`access_token` key and copy its value without the enclosing quotes.

Set the environment variable `access_token` to this value.


## Usage

### `f4fctl deploy <yaml pathname>`

Deploy an experiment in the cluster. The YAML file must follow the
format of example_pipeline.yaml.


### `f4fctl status [all]`

Print the status of all pods that have been instantiated by the f4f
operator. If the optional argument `all` is given, then print the
status of all pods in the cluster.


### `f4fctl show dir <path>`

Prints a directory listing from the volume where pipelines store their
final results, that is, the outputs of the final step in the pipeline.


### `f4fctl show file <pathname>`

Prints the contents of a file from the volume where pipelines store
their final results, that is, the outputs of the final step in the
pipeline.


### `f4fctl fetch <remote pathname> <local pathname>`

Downloads a file from the volume where pipelines store their final
results, that is, the outputs of the final step in the pipeline.
